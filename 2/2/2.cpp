﻿#include <iostream>
#include <cmath>
#include <iomanip>

using namespace std;

double f(double x) {
    return pow(x, 3.0 / 2.0);
}

double f_taylor(double x, double a, double eps) {
    double sum = 0;
    double term = 0;
    double da = x - a;
    int n = 0;

    while (fabs(term) >= eps) {
        sum += term;
        term *= da * (3.0 / 2.0 - n) / (n + 1);
        n++;
    }
    return f(a) + sum;
}

int main() {
    double a = 4.0;
    double b = 5.0;
    double eps = 1e-4;
    int m = 10;

    cout << "a, b, eps: " << a << ", " << b << ", " << eps << endl;
    cout << "Amount of points m: " << m << endl;

    cout << "|" << setfill('-') << setw(104) << "-" << setfill(' ') << "|" << endl;
    cout << "| " << setw(10) << "x"
        << " | " << setw(20) << "f(x)"
        << " | " << setw(20) << "f_taylor(x)"
        << " | " << setw(20) << "Accuracy"
        << " | " << setw(20) << "n iterations" << " |" << endl;
    cout << "|" << setfill('-') << setw(104) << "-" << setfill(' ') << "|" << endl;

    for (int i = 0; i < m; i++) {
        double x = a + i * (b - a) / (m - 1);
        double f_val = f(x);
        double f_taylor_val = f_taylor(x, x, eps);
        double accuracy = fabs(f_val - f_taylor_val);
        int iterations = 0;

        double term = 1;
        while (fabs(term) >= eps) {
            double n = 0.0;
            term *= (x - x) * (3.0 / 2.0 - n) / (n + 1);
            n++;
            iterations++;
        }

        cout << "| " << setw(10) << x
            << " | " << setw(20) << f_val
            << " | " << setw(20) << f_taylor_val
            << " | " << setw(20) << accuracy
            << " | " << setw(20) << iterations << " |" << endl;
        cout << "|" << setfill('-') << setw(104) << "-" << setfill(' ') << "|" << endl;
    }

    return 0;
}